package com.baeldung.birt.engine.controller;

import com.baeldung.birt.engine.dto.Image;
import com.baeldung.birt.engine.dto.Report;
import com.baeldung.birt.engine.dto.Template;
import com.baeldung.birt.engine.service.BirtReportService;
import org.eclipse.birt.report.engine.api.EngineException;

import io.agroal.api.AgroalDataSource;
import org.eclipse.microprofile.metrics.annotation.SimplyTimed;
import org.jboss.logging.Logger;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.inject.Inject;

@Path("/report")
public class BirtReportController {

    private static final Logger LOG = Logger.getLogger(BirtReportController.class);

    @Inject
    BirtReportService reportService;

    @Inject
    AgroalDataSource defaultDataSource;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("templates")
    @SimplyTimed
    public List<Template> getTemplates() {
        return reportService.getTemplates();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("images")
    @SimplyTimed
    public List<Image> getImages() {
        return reportService.getImages();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<Report> listReports() {
        return reportService.getReports();
    }

    @Path("reload")
    @GET
    public Response reloadReports() {
        try {
            reportService.loadReports();
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }
        return Response.ok("OK").build();
    }

    @Path("{name}")
    @Produces("application/pdf")
    @GET
    public Response generateFullReport(@PathParam("name") String name) {
        return reportService.generatePDFReport(name);
    }
}
