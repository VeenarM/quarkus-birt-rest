package com.baeldung.birt.engine.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.BeforeDestroyed;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.baeldung.birt.engine.dto.Image;
import com.baeldung.birt.engine.dto.Report;

import com.baeldung.birt.engine.dto.Template;
import io.agroal.api.AgroalDataSource;
import io.quarkus.scheduler.Scheduled;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.HTMLServerImageHandler;
import org.eclipse.birt.report.engine.api.IGetParameterDefinitionTask;
import org.eclipse.birt.report.engine.api.IParameterDefn;
import org.eclipse.birt.report.engine.api.IRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.SimplyTimed;
import org.jboss.logging.Logger;

@ApplicationScoped
public class BirtReportService {

    private static final Logger LOG = Logger.getLogger(BirtReportService.class);

    @Inject
    AgroalDataSource defaultDataSource;

    private IReportEngine birtEngine;
    private Map<String, IReportRunnable> reports;
    private List<Template> templates;
    private List<Image> images = new ArrayList<>();

    @PostConstruct
    @SuppressWarnings("unchecked")
    protected void initialize() throws BirtException, IOException {
        EngineConfig config = new EngineConfig();
        config.getAppContext().put("quarkus", this.getClass());
        Platform.startup(config);
        IReportEngineFactory factory = (IReportEngineFactory) Platform.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
        birtEngine = factory.createReportEngine(config);

        // On Init setup reports/images and run initial generation of service just to warm up BIRT.
        loadReports();
        loadImages();
        generatePDFReport("static_report");
    }

    /**
     * Load report files to memory from db every hour.
     * As this is scheduled it's auto loaded as part of PostConstruction.
     */
    @Scheduled(every = "1h", delay = 1, delayUnit = TimeUnit.HOURS)
    @SimplyTimed
    public void loadReports() throws EngineException, IOException {
        LOG.info("Reloading reports from database into memory...");
        List<Template> t = new ArrayList<>();
        try (Connection connection = defaultDataSource.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT * FROM AIR_TEMPLATES")) {
            ps.execute();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                Template template = Template.builder().index(rs.getInt("idx")).name(rs.getString("name")).xmlData(rs.getString("data")).build();
                t.add(template);
            }
        } catch (Exception exception) {
            LOG.error("Exception occurred in getting templates, no reload occurred...", exception);
            // TODO send an email notification? or webhook?
            return;
        }

        Map<String, IReportRunnable> newReports = new HashMap<>();
        if (!t.isEmpty()) {
            for (Template templ : t) {
                try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(templ.getXmlData().getBytes(StandardCharsets.UTF_8))) {
                    newReports.put(templ.getName().replace(".rptdesign", ""), birtEngine.openReportDesign(byteArrayInputStream));
                }
            }
        } else {
            // TODO - Templates are empty on retrieval? do we want to burn here? or make it update to zero reports available?
        }

        templates = Collections.unmodifiableList(t);
        reports = Collections.unmodifiableMap(newReports);
    }

    @Scheduled(every = "1h", delay = 1, delayUnit = TimeUnit.HOURS)
    @SimplyTimed
    public void loadImages() {
        LOG.info("Reloading images from database into memory...");
        List<Image> images = new ArrayList<>();
        try (Connection connection = defaultDataSource.getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT * FROM AIR_IMAGES")) {
            ps.execute();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                images.add(Image.builder().index(rs.getInt("idx")).name(rs.getString("name")).binaryData(rs.getString("data")).build());
            }
        } catch (Exception exception) {
            LOG.error("Exception occurred in getting images, no reload occurred...", exception);
            // TODO send an email notification? or webhook?
            return;
        }
        if (!images.isEmpty()) {
            this.images = Collections.unmodifiableList(images);
        }
    }

    public List<Template> getTemplates() {
        return this.templates;
    }

    public List<Image> getImages() {
        return this.images;
    }

    /**
     * This loops our DB entries and pulls the title out of the template XML for display.
     * @return
     */
    public List<Report> getReports() {
        List<Report> response = new ArrayList<>();
        for (Map.Entry<String, IReportRunnable> entry : reports.entrySet()) {
            IReportRunnable report = reports.get(entry.getKey());
            IGetParameterDefinitionTask task = birtEngine.createGetParameterDefinitionTask(report);
            Report reportItem = new Report(report.getDesignHandle().getProperty("title").toString(), entry.getKey());
            for (Object h : task.getParameterDefns(false)) {
                IParameterDefn def = (IParameterDefn) h;
                reportItem.getParameters().add(new Report.Parameter(def.getPromptText(), def.getName(), getParameterType(def)));
            }
            response.add(reportItem);
        }
        return response;
    }

    private Report.ParameterType getParameterType(IParameterDefn param) {
        if (IParameterDefn.TYPE_INTEGER == param.getDataType()) {
            return Report.ParameterType.INT;
        }
        return Report.ParameterType.STRING;
    }

    /**
     * Generate a report as PDF
     */
    public Response generatePDFReport(String reportName) {
        IReportRunnable report = reports.get(reportName);
        IRunAndRenderTask runAndRenderTask = birtEngine.createRunAndRenderTask(report);
        IRenderOption options = new RenderOption();
        PDFRenderOption pdfRenderOption = new PDFRenderOption(options);
        pdfRenderOption.setOutputFormat("pdf");
        runAndRenderTask.setRenderOption(pdfRenderOption);

//        runAndRenderTask.getAppContext().put(EngineConstants.APPCONTEXT_PDF_RENDER_CONTEXT, "request");

        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            pdfRenderOption.setOutputStream(output);
            runAndRenderTask.run();

//             reportContext.getDesignHandle().findStyle("BackgroundStyle").backgroundImage = "watermarkpdf.png";

            Response.ResponseBuilder responseBuilder = Response.ok(output.toByteArray());
            responseBuilder.type(birtEngine.getMIMEType("pdf"));
            // responseBuilder.header("Content-Disposition", "filename=test.pdf");
            return responseBuilder.build();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            runAndRenderTask.close();
        }
    }

    @PreDestroy
    public void destroy() {
        reports.clear();
        birtEngine.destroy();
        Platform.shutdown();
    }
}