package com.baeldung.birt.engine.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.io.UnsupportedEncodingException;

@Getter
@Builder
public class Template {

    @NonNull
    private int index;
    @NonNull
    private String name;
    @NonNull
    private String xmlData;


    public int getDataSize() throws UnsupportedEncodingException {
        return this.getXmlData().getBytes("UTF-8").length;
    }

}
