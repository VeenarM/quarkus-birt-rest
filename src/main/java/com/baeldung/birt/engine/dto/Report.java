package com.baeldung.birt.engine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Report DTO class
 * This class holds the Report Object and it's parameters to be passed into the Birt Generator.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Report {
    private String title;
    private String name;
    private List<Parameter> parameters;

    public Report(String title, String name) {
        this.title = title;
        this.name = name;
    }

    @Data
    @AllArgsConstructor
    public static class Parameter {
        private String title;
        private String name;
        private ParameterType type;
    }

    public enum ParameterType {
        INT, STRING
    }
}