package com.baeldung.birt.engine.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.io.UnsupportedEncodingException;

@Getter
@Builder
public class Image {

    @NonNull
    private int index;
    @NonNull
    private String name;
    @NonNull
    private String binaryData;


    public int getDataSize() throws UnsupportedEncodingException {
        return this.getBinaryData().getBytes("UTF-8").length;
    }

}
