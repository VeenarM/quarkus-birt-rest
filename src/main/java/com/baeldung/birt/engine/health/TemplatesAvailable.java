package com.baeldung.birt.engine.health;

import com.baeldung.birt.engine.service.BirtReportService;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Readiness
@ApplicationScoped
public class TemplatesAvailable implements HealthCheck {

    @Inject
    BirtReportService birtReportService;

    @Override
    public HealthCheckResponse call() {
        if (birtReportService.getReports().isEmpty()) {
            return HealthCheckResponse.builder().name("Templates Available").down().build();
        } else {
            return HealthCheckResponse.builder().name("Templates Available").up().withData("Reports Available", birtReportService.getReports().size()).build();
        }
    }
}
